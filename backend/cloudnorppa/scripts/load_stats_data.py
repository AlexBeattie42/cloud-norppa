import pandas as pd

from cloudnorppa.models import Region, Statistic, Source

df = pd.read_csv(r'./data/DataToTrack.csv')
print(df.columns)
for index, row in df.iterrows():
    region = Region.objects.get(pk=row['Location'])
    # Emissions Stat
    if (not pd.isna(row['Converted_Value'])) and (row['Converted_Value'] != 0.0):
        print(row['Location'], row['Converted_Value'])

        source = Source(url=row['Source_Emissions'],
                        name=f"{row['Location']} Emissions",
                        year="2020" if pd.isna(row['Year']) else row['Year'])
        source.save()

        stat = Statistic(region=region,
                         type=Statistic.Type.CO2_EMISSIONS,
                         value=row['Converted_Value'],
                         unit=Statistic.Unit.GRAMS_KW_HOUR)
        stat.save()
        stat.source.add(source)
        stat.save()
    # Percent renewable
    if (not pd.isna(row['Pcnt_Renew'])) and (row['Pcnt_Renew'] != 0.0):
        print(row['Location'], row['Pcnt_Renew'])

        source = Source(url=row['Source_Renewable'],
                        name=f"{row['Location']} Emissions",
                        year="2020" if pd.isna(row['Year']) else row['Year'])
        source.save()

        stat = Statistic(region=region,
                         type=Statistic.Type.PERCENT_RENEWABLES,
                         value=row['Pcnt_Renew'],
                         unit=Statistic.Unit.PERCENT)
        stat.save()
        stat.source.add(source)
        stat.save()
