from decimal import Decimal

from cloudnorppa.models import Statistic, Grade, Region

continents = ['europe', 'north america', 'africa', 'southeast asia', 'east asia', 'australia']
for continent in continents:
    # Calculate Grades for C02
    statistics = Statistic.objects.filter(type=Statistic.Type.CO2_EMISSIONS, region__geo_region=continent).order_by('-value')
    max_val = statistics.first().value
    min_val = statistics.last().value
    print(f"Continent: {continent} Min Val: {min_val} Max Val: {max_val}")

    # region_test = Region.objects.filter(name="Germany")
    # print(region_test.first().statistic_set.all())

    for stat in statistics:
        if min_val == max_val:
            percent_val = 0.5
        else:
            percent_val = 1 - ((stat.value - min_val) / (max_val - min_val))
        stat.grade = percent_val
        stat.save()
        print(f"Name: {stat.region.name} Percent: {percent_val}")

for continent in continents:
    # Calculate Grades for % Renewables
    statistics = Statistic.objects.filter(type=Statistic.Type.PERCENT_RENEWABLES, region__geo_region=continent).order_by('-value')
    max_val = statistics.first().value
    min_val = statistics.last().value
    print(f"Continent: {continent} Min Val: {min_val} Max Val: {max_val}")

    for stat in statistics:
        if min_val == max_val:
            percent_val = 0.5
        else:
            percent_val = ((stat.value - min_val) / (max_val - min_val))
        stat.grade = percent_val
        stat.save()
        print(f"Name: {stat.region.name} Percent: {percent_val}")

regions = Region.objects.all()

for region in regions:
    print(region.name)
    stat_value = 0
    stats = region.statistics.all()

    for stat in stats:
        if stat.grade:
            if stat.type == Statistic.Type.CO2_EMISSIONS:
                stat_value += stat.grade * Decimal(0.75)
            else:
                stat_value += stat.grade * Decimal(0.25)


    grades = Grade.objects.all()
    for letter_grade in grades:
        if stat_value >= letter_grade.value:
            region.grade = letter_grade
            break
        else:
            region.grade = Grade.objects.get(pk=Grade.Letter.F)
    region.save()
    print(f"Name: {region.name} Grade: {region.grade.name}")
