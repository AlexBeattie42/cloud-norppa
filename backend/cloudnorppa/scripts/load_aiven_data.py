import json
import urllib.request

from cloudnorppa.models import Cloud, Region

AIVEN_API_BASE = "https://api.aiven.io/v1"

with urllib.request.urlopen(f"{AIVEN_API_BASE}/clouds") as url:
    data = json.loads(url.read().decode())
    clouds = data["clouds"]
    print(clouds)

    for cloud in clouds:
        head, sep, tail = cloud["cloud_name"].partition('-')
        print(head)
        cloud_obj = Cloud(name=cloud["cloud_name"],
                          description=cloud["cloud_description"],
                          geo_latitude=cloud["geo_latitude"],
                          geo_longitude=cloud["geo_longitude"],
                          provider=head
                          )

        cloud_obj.save()

        start = ', '
        end = ' -'
        s = cloud["cloud_description"]
        country = s[s.find(start)+len(start):s.rfind(end)]
        region_obj = Region(name=country,
                            abbreviation="NA",
                            geo_region=cloud["geo_region"])
        region_obj.save()
        region_obj.cloud.add(cloud_obj)
        region_obj.save()



