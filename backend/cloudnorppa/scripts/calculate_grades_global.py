
from cloudnorppa.models import Statistic, Grade, Region

# Calculate Grades for C02
statistics = Statistic.objects.filter(type=Statistic.Type.CO2_EMISSIONS).order_by('-value')
max_val = statistics.first().value
min_val = statistics.last().value

# region_test = Region.objects.filter(name="Germany")
# print(region_test.first().statistic_set.all())

grades = Grade.objects.all()
for stat in statistics:
    percent_val = 1 - ((stat.value - min_val) / (max_val - min_val))
    for letter_grade in grades:
        if percent_val >= letter_grade.value:
            stat.grade = letter_grade
            break
        else:
            stat.grade = Grade.objects.get(pk=Grade.Letter.F)
    stat.save()
    print(f"Name: {stat.region.name} Percent: {percent_val} Grade: {stat.grade.name}")

# Calculate Grades for % Renewables
statistics = Statistic.objects.filter(type=Statistic.Type.PERCENT_RENEWABLES).order_by('-value')
max_val = statistics.first().value
min_val = statistics.last().value


for stat in statistics:
    percent_val = ((stat.value - min_val) / (max_val - min_val))
    stat.grade = percent_val
    stat.save()
    print(f"Name: {stat.region.name} Percent: {percent_val} Grade: {stat.grade.name}")

regions = Region.objects.all()

for region in regions:
    stat_value = 0
    stats = region.statistics.all()
    # Equally weight each statistic
    count = stats.count()
    for stat in stats:
        stat_value += stat.grade * 1/count

    grades = Grade.objects.all()
    for letter_grade in grades:
        if percent_val >= letter_grade.value:
            region.grade = letter_grade
            break
        else:
            region.grade = Grade.objects.get(pk=Grade.Letter.F)
    region.save()
    print(f"Name: {region.name} Grade: {region.grade.name}")

