from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins

from cloudnorppa.models import Region
from cloudnorppa.serializers import RegionStatsSerializer, RegionSerializer


class RegionsStatsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows regions to be viewed.
    """
    queryset = Region.objects.all().order_by('grade')
    serializer_class = RegionStatsSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['geo_region', 'grade']

class RegionsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows regions to be viewed.
    """
    queryset = Region.objects.all().order_by('grade')
    serializer_class = RegionSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['geo_region', 'grade']