from django.apps import AppConfig


class CloudNorppaConfig(AppConfig):
    name = 'cloudnorppa'
    verbose_name = 'cloudNorppa Is Great'
