from rest_framework import serializers
from django_typomatic import ts_interface, generate_ts
from cloudnorppa.models import Region, Cloud, Statistic, Source


@ts_interface()
class CloudSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cloud
        fields = '__all__'
        lookup_field = 'name'


@ts_interface()
class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = '__all__'


@ts_interface()
class StatisticSerializer(serializers.ModelSerializer):
    source = SourceSerializer(many=True, read_only=True)

    class Meta:
        model = Statistic
        fields = '__all__'

    @staticmethod
    def get_queryset():
        return Statistic.objects.prefetch_related('source').all()


@ts_interface()
class RegionStatsSerializer(serializers.ModelSerializer):
    statistics = StatisticSerializer(many=True, read_only=True)
    cloud = CloudSerializer(many=True, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        lookup_field = 'name'

    @staticmethod
    def get_queryset():
        return Region.objects.prefetch_related('statistics', 'cloud').all()


@ts_interface()
class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ['name', 'geo_region', 'grade']
        lookup_field = 'name'
