from django.db import models
from django.utils.translation import gettext_lazy as _


class Cloud(models.Model):
    class Provider(models.TextChoices):
        AWS = 'aws'
        AZURE = 'azure'
        GOOGLE = 'google'
        DIGITAL_OCEAN = 'do'
        UPCLOUD = 'upcloud'

    name = models.CharField(max_length=30, primary_key=True)
    description = models.CharField(max_length=120)
    geo_latitude = models.DecimalField(max_digits=10, decimal_places=5, default=0.0)
    geo_longitude = models.DecimalField(max_digits=10, decimal_places=5, default=0.0)
    provider = models.CharField(
        max_length=10,
        choices=Provider.choices,
        null=True
    )


class Grade(models.Model):
    class Letter(models.TextChoices):
        A = 'A'
        B = 'B'
        C = 'C'
        D = 'D'
        E = 'E'
        F = 'F'

    name = models.CharField(
        max_length=1,
        choices=Letter.choices,
        default=Letter.F,
        primary_key=True
    )
    value = models.DecimalField(max_digits=19, decimal_places=10)


class Region(models.Model):
    name = models.CharField(max_length=60, primary_key=True)
    abbreviation = models.CharField(max_length=30)
    geo_region = models.CharField(max_length=30)
    cloud = models.ManyToManyField(Cloud)
    grade = models.ForeignKey(Grade, on_delete=models.CASCADE, null=True, related_name="region")


class Source(models.Model):
    url = models.CharField(max_length=512)
    name = models.CharField(max_length=60)
    year = models.IntegerField(default=0)


class Statistic(models.Model):
    class Meta:
        unique_together = ('region', 'type')

    class Unit(models.TextChoices):
        GRAMS_KW_HOUR = 'GKWH', _('gCo2/kWh')
        PERCENT = 'PCNT', _('%')

    class Type(models.TextChoices):
        CO2_EMISSIONS = 'CO2'
        PERCENT_RENEWABLES = 'PREN'

    region = models.ForeignKey(Region, on_delete=models.CASCADE, related_name="statistics")
    type = models.CharField(max_length=4, choices=Type.choices, default=Type.CO2_EMISSIONS)
    value = models.DecimalField(max_digits=19, decimal_places=10)
    unit = models.CharField(
        max_length=4,
        choices=Unit.choices,
        default=Unit.GRAMS_KW_HOUR,
    )
    source = models.ManyToManyField(Source)
    grade = models.DecimalField(max_digits=19, decimal_places=10, null=True)

#
# class ContinentStats(models.Model):
#     name = models.CharField(max_length=60, primary_key=True)
#     max_value_co2 = models.DecimalField(max_digits=19, decimal_places=10, null=True)
#     min_value_co2 = models.DecimalField(max_digits=19, decimal_places=10, null=True)
