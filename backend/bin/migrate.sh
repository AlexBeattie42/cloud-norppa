#!/bin/bash

echo "Creating Migrations"

# Make migrations if necessary
python manage.py makemigrations cloudnorppa

# Run migrations if necessary
python manage.py migrate

# Load fixtures
python manage.py loaddata grades.json