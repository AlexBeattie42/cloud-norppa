### gatsby-admin-template

Admin dashboard template based on Gatsby with [@paljs/ui](https://github.com/paljs/ui) component package

#### Setup:

```
git clone https://github.com/paljs/gatsby-admin-template.git

cd gatsby-admin-template

yarn install

yarn dev
```

#### Docker:

```
git clone https://github.com/paljs/gatsby-admin-template.git

cd gatsby-admin-template

docker build -t gatsby-admin-template

docker run --rm -d -p 80:80 gatsby-admin-template
```

![screenshot](srcmages/screenshot1.png)

![screenshot](srcmages/screenshot2.png)

![screenshot](srcmages/screenshot3.png)

![screenshot](srcmages/screenshot4.png)
