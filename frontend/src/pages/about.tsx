import React from 'react';
import SEO from '../components/SEO';
import Row from "@paljs/ui/Row";
import Col from "@paljs/ui/Col";
import {Card, CardBody, CardFooter, CardHeader} from "@paljs/ui/Card";
import {Box} from "./index";


const About = () => {
    return (
        <div>
            <SEO title="About Us"/>
            <Row>
                <Col breakPoint={{xs: 12, md: 6}}>
                    <Card size="Large">
                        <CardHeader>Team Norppa!</CardHeader>

                        <CardBody>
                            <Row center="xs">
                                <Box nested>
                                    <img width="100%" src="/team.jpg" alt="Cloud Norppa"/>
                                </Box>

                            </Row>
                        </CardBody>
                    </Card>
                </Col>
                <Col breakPoint={{xs: 12, md: 6}}>
                    <Card size="Large">
                        <CardHeader>Our Story</CardHeader>
                        <CardBody>
                            <p>Team of four ambitious, curious, and adventurous MSc. students from LUT University in
                                Lappeenranta, Finland. Our team offers a global perspective with members from Finland,
                                the
                                USA, and Mexico. We have diverse backgrounds in computer science, engineering, data
                                analytics, and business that allows us to approach problems from multiple perspectives.
                                Most
                                importantly, we are a fun-loving team always seeking new challenges, thrills, and not
                                afraid
                                of crazy ideas. For example, the team name “Norppa” which means ringed-seal in Finnish,
                                came
                                from our shared enjoyment of Finnish avanto or ice swimming. This fits particularly well
                                with the sustainable aspect of this project, as Norppas (Saimaa Ringed Seals) are
                                endangered
                                freshwater seals that live only in lake Saimaa in Lappeenranta and surrounding
                                areas. </p>
                            <p>(from left to right)
                                Markus Latvakoski – MSc. Industrial Engineering Management Student, Data
                                Analytics
                                Alex Beattie – MSc. Mechatronics Student, DevOps Engineer, and Researcher
                                Lexi Lobdell – MSc. International Business & Entrepreneurship Student,
                                Researcher
                                Diego Mendiola – MSc. Industrial Engineering Management Student, Aerospace
                                Engineer</p>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>

    );
};
export default About;
