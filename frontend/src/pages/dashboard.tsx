import React, {useState} from 'react';
import SEO from '../components/SEO';
import {Checkbox} from '@paljs/ui/Checkbox';
import {Card, CardBody} from '@paljs/ui/Card';
import Row from '@paljs/ui/Row';
import Col from '@paljs/ui/Col';
import {Box, List, ListItem} from "@material-ui/core";
import {Select} from "@paljs/ui";
import axios from "axios";
import {RegionStatsSerializer} from "../model/model";

const grade = ["A", "B", "C", "D", "E", "F"];
const continent: { value: any; label: any }[] = [
    {value: 'europe', label: 'Europe'},
    {value: 'north america', label: 'North America'},
    {value: 'africa', label: 'Africa'},
    {value: 'southeast asia', label: 'Southeast Asia'},
    {value: 'east asia', label: 'East Asia'},
    {value: 'australia', label: 'Oceania'},
];
const initState: any = {};
const initRegions: RegionStatsSerializer[] = [];
// const [regions, setStarsCount] = useState(0)

grade.forEach((key) => (initState[key] = false));

const calculate_stats = (regions: RegionStatsSerializer[]) => {
    console.log(regions)
    return regions;
}

const Home = () => {
    const [checkbox, setCheckbox] = useState(initState);
    const [regions, setRegions] = useState(initRegions)
    const onChangeCheckbox = (value: boolean, key: string) => {
        setCheckbox({...checkbox, [key]: value});
        console.log(key, value);
    };
    const onContinentChange = (key: any) => {
        axios.get<RegionStatsSerializer[]>(`${process.env.API_URL}/regions/?geo_region=${key}&ordering=grade`)
            .then((response) => {
                setRegions(calculate_stats(response.data));
            }).catch(error => {
            console.log(error.response)
        })
    };
    return (
        <div>
            <SEO title="Home"/>
            <Card size="Giant">
                <header>
                    <Select onChange={(pair) => onContinentChange(pair.value)} options={continent}
                            placeholder="Select a Continent"/>
                </header>
                <CardBody>
                    <Row>
                        {/*<Col breakPoint={{xs: 4, sm: 3, md: 2, lg: 1}}>*/}
                        {/*    <Box>*/}
                        {/*        Grade*/}
                        {/*        <List>*/}
                        {/*            {grade.map((key, index) => (*/}
                        {/*                <ListItem key={index}>*/}
                        {/*                    <Checkbox checked={checkbox[key]} status={key}*/}
                        {/*                              onChange={(value) => onChangeCheckbox(value, key)}>*/}
                        {/*                        {key}*/}
                        {/*                    </Checkbox>*/}
                        {/*                </ListItem>*/}
                        {/*            ))}*/}
                        {/*        </List>*/}
                        {/*    </Box>*/}
                        {/*</Col>*/}
                        <Col breakPoint={{xs: true}}>
                            <Row>
                                {regions.map((region, index) => (
                                    <Col key={index} breakPoint={{md: true}}>
                                        <Card>
                                            <CardBody>
                                                <Row center="xs">
                                                    <img
                                                        style={{marginRight: '6em'}}
                                                        height="20px" src={`/flags/${region.grade}.png`} alt="Flag"/>
                                                </Row>
                                                <Row center="xs">
                                                    <img
                                                        style={{
                                                            filter: "invert(1)",
                                                            marginLeft: '4em',
                                                            marginRight: '4em'
                                                        }}
                                                        width="72px" src="/data_center.png" alt="Data Center"/>
                                                </Row>
                                                <p style={{textAlign: "center"}}>{region.name}</p>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                ))}
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    );
};
export default Home;
