export interface CloudSerializer {
    name: string;
    description: string;
    geo_latitude?: number;
    geo_longitude?: number;
    provider?: any | null;
}

export interface SourceSerializer {
    id?: number;
    url: string;
    name: string;
    year?: number;
}

export interface StatisticSerializer {
    id?: number;
    source?: SourceSerializer[];
    type?: any;
    value: number;
    unit?: any;
    region: any;
    grade?: any | null;
}

export interface RegionStatsSerializer {
    name: string;
    statistics?: StatisticSerializer[];
    cloud?: CloudSerializer[];
    abbreviation: string;
    geo_region: string;
    grade: string;
}

